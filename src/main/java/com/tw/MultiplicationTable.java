package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (isValid(start, end)) {
            return generateTable(start, end);
        }
        return null;
    }

    public Boolean isValid(int start, int end) {
        return isStartNotBiggerThanEnd(start, end) && isInRange(start) && isInRange(end);
    }

    public Boolean isInRange(int number) {
        return number>=1 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        String table = "";
        for (int row = start; row <= end; row++) {
            table += generateLine(start, row) + "%n";
        }
        String newTable = String.format(table);
        return newTable.trim();
    }

    public String generateLine(int start, int row) {
        String line = "";
        for (int current = start; current <= row; current++) {
            line += generateSingleExpression(current, row) + "  ";
        }
        String newLine = line.trim();
        return newLine;
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        String expression = multiplicand + "*" + multiplier  + "=" + (multiplicand * multiplier);
        return expression;
    }
}
